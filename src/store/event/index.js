import axios from 'axios'

export default {
  state: {
    eventLoading: false,
    loadedEvents: []
  },
  mutations: {
    setLoadedEvents (state, payload) {
      state.loadedEvents = payload
    },
    createEvent (state, payload) {
      const newEvent = payload
      // On génère l'id de la même façon que le serveur temporairement pour avoir la bonne image
      newEvent._id = Math.max.apply(Math, state.loadedEvents.map(event => (event._id + 1)))
      state.loadedEvents.push(newEvent)
    }
  },
  actions: {
    loadEvents ({ commit }) {
      axios.get('/events')
      .then(res => {
          commit('setLoadedEvents', res.data.result)
        })
        .catch(err => console.log(err))
    },
    createEvent ({commit}, newEvent) {
      axios.post('/events', newEvent)
        .then(res => {
          commit('createEvent', newEvent)
          console.log(res.data)
        })
        .catch(err => console.log(err.response.data))
    }
  },
  getters: {
    loadedEvents (state) {
      return state.loadedEvents.sort((eventA, eventB) => {
        return eventA.date > eventB.date
      })
    },
    loadedEvent (state) {
      return eventId => {
        return state.loadedEvents.find(event => {
          return event._id == eventId
        })
      }
    },
    featuredEvents (state, getters) {
      return getters.loadedEvents.slice(0, 3)
    },
    filteredEvents (state) {
      return array => {
        return state.loadedEvents.filter(event => {
          return array.includes(event._id.toString())
        })
      }
    },
    createdByEvents (state) {
      return userId => {
        return state.loadedEvents.filter(event => {
          return event.creatorId == userId
        })
      }
    }
  }
}
