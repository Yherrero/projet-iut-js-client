import axios from 'axios'

export default {
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    loggedUser: null,
    loadedUsers: []
  },
  mutations: {
    setLoadedUsers (state, payload) {
      state.loadedUsers = payload
    },
    setLoggedUser (state, payload) {
      state.loggedUser = payload
    },
    registerUser (state, payload) {
      state.loggedUser.goingTo.push(payload.eventId.toString())
    },
    unregisterUser (state, payload) {
      state.loggedUser.goingTo = state.loggedUser.goingTo.filter(el => (el != payload.eventId))
    },
    auth_request (state) {
      state.status = 'loading'
    },
    auth_success (state, user) {
      state.status = 'success'
      state.token = user.token
      state.loggedUser = user.user
    },
    auth_error (state) {
      state.status = 'error'
    },
    logout (state) {
      state.status = ''
      state.token = ''
      state.loggedUser = null
      state.loadedUsers = []
    }
  },
  actions: {
    loadUsers ({ commit }) {
      axios.get('/users')
      .then(res => {
          commit('setLoadedUsers', res.data.result)
        })
        .catch(err => console.log(err))
    },
    loadLoggedUser ({commit}, userId) {
      axios.get('/users/' + userId)
        .then(res => {
          commit('setLoggedUser', res.data.user)
        })
        .catch(err => console.log(err.response))
    },
    registerUser ({commit, getters},  eventId) {
      const userId = getters.loggedUser._id
      axios.patch(`/users/register/${userId}/${eventId}`)
        .then(res => {
          commit('registerUser', {
            userId,
            eventId
          })
        })
        .catch(err => console.log(err.response.data))
    },
    unregisterUser ({commit, getters},  eventId) {
      const userId = getters.loggedUser._id
      axios.patch(`/users/unregister/${userId}/${eventId}`)
        .then(res => {
          commit('unregisterUser', {
            userId,
            eventId
          })
        })
        .catch(err => console.log(err.response.data))
    },
    login ({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios.post('/users/login', user)
          .then(res => {
            const token = ('bearer ' + res.data.token)
            const user = res.data.user
            localStorage.setItem('token', token)
            axios.defaults.headers.common['Authorization'] = token
            commit('auth_success', { token, user })
            resolve(res)
          })
          .catch(err => {
            commit('auth_error')
            localStorage.removeItem('token')
            reject(err)
          })
      })
    },
    register ({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios.post('/users/signup', user)
          .then(res => {
            const token = ('bearer ' + res.data.token)
            const user = res.data.user
            localStorage.setItem('token', token)
            axios.defaults.headers.common['Authorization'] = token
            commit('auth_success', { token, user })
            resolve(res)
          })
          .catch(err => {
            commit('auth_error')
            localStorage.removeItem('token')
            reject(err)
          })
      })
    },
    logout ({ commit }) {
      return new Promise((resolve, reject) => {
        commit('logout')
        localStorage.removeItem('token')
        delete axios.defaults.headers.common['Authorization']
        resolve()
      })
    }
  },
  getters: {
    loggedIn: state => !!state.token,
    authStatus: state => state.status,
    token: state => state.token,
    loggedUser: state => state.loggedUser,
    loadedUsers (state) {
      return state.loadedUsers
    },
    loadedUser (state) {
      return userId => {
        return state.loadedUsers.find(user => user._id == userId)
      }
    }
  }
}
