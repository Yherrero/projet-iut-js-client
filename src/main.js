import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import jwtDecode from 'jwt-decode'
import './registerServiceWorker'

Vue.config.productionTip = false

// axios.defaults.baseURL = 'http://127.0.0.1:3000'
axios.defaults.baseURL = 'https://js-project-iut.herokuapp.com'

Vue.prototype.$http = axios
const token = localStorage.getItem('token')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

new Vue ({
  router,
  store,
  render: h => h(App),
  created () {
    this.$store.dispatch('loadEvents')
    if (token) {
      this.$store.dispatch('loadUsers')
      const user = jwtDecode(token)
      this.$store.dispatch('loadLoggedUser', user.id)
    }
  }
}).$mount('#app')
