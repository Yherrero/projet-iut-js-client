import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/shared/Home'
import Login from './views/shared/Login'
import Signup from './views/shared/Signup'
import Event from './views/event/Event'
import Events from './views/event/Events'
import EventEdit from './views/event/EventEdit'
import NewEvent from './views/event/NewEvent'
import Profil from './views/user/Profil'
import store from './store'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/edit/event/:id',
      name: 'edit event',
      component: EventEdit,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/event/:id',
      name: 'event',
      component: Event,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/events',
      name: 'events',
      component: Events
    },
    {
      path: '/new-event',
      name: 'new-event',
      component: NewEvent,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        requiresGuest: true
      }
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup,
      meta: {
        requiresGuest: true
      }
    },
    {
      path: '/profil',
      name: 'profil',
      component: Profil,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '*',
      name: 'notFound',
      component: Home
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.loggedIn) {
      next()
      return
    }
    next('/login')
  } else if (to.matched.some(record => record.meta.requiresGuest)) {
    if (store.getters.loggedIn) {
      next('/')
      return
    }
    next()
  } else {
    next()
  }
})

export default router
